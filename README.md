# About this project

I made this tiny project as a code sample to show my style of coding. I focussed on rebuilding a small part of an Unreal Engine system which I have grown to love. It handles input through a controller, which only gives access to the input when certain requirements are met. This abstraction makes it very easy to decouple player and AI behaviour, which is really nice and cool :)

The scripts that implement this feature are located here:
Assets/Scripts/Player/PlayerController.cs
Assets/Scripts/Player/Avatar.cs

With an simple script implementing it located here:
Assets/Character/CharacterMovement.cs

With a slightly more complicated system implementing it located here:
Assets/Combat/Weapons/Weapon.cs
Assets/Combat/Weapons/Pistol.cs

I'll be updating this in my free time to actually make a game that utilizes this.
