﻿using System;
using Player;
using UnityEngine;

namespace Character
{
    /**
     * A simple behaviour that allows a object to move around
     */
    public class CharacterMovement : MonoBehaviour, IInputReceiver
    {
        /** The cached component that is actually performing the movement logic */
        private CharacterController _cachedCharacterController;
        
        /** The cached input direction the object wants to move towards */
        private Vector3 _inputDirection = Vector3.zero;
        
        /** The maximum speed at which the object can move */
        [SerializeField] private float moveSpeed = 1.0f;
        
        //~ START IInputReceiver implementation 
        public void ReceiveAxis(in string inputName, float axisValue)
        {
            if (inputName == "Horizontal")
            {
                _inputDirection.x = axisValue;
                return;
            } 
            if (inputName == "Vertical")
            {
                _inputDirection.z = axisValue;
                return;
            }
        }
        //~ END IInputReceiver implementation

        private void Start()
        {
            _cachedCharacterController = gameObject.GetComponent<CharacterController>();
            if (_cachedCharacterController == null)
            {
                enabled = false;
                Debug.LogError($"{gameObject.name} tried starting CharacterMovement, but gameObject has no CharacterController behaviour");
                return;
            }
            InitializeInput();
        }

        private void Update()
        {
            MoveTowardsFacingDirection(transform);
        }

        /** Updates the gameObject position towards a transform's facing direction
         * @param facingTransform - The transform to use as the forward vector
         */
        protected void MoveTowardsFacingDirection(in Transform facingTransform)
        {
            Vector3 forwardInput = _inputDirection.z * facingTransform.forward;
            Vector3 rightInput = _inputDirection.x * facingTransform.right;
            Vector3 movement = (forwardInput + rightInput).normalized;
            _cachedCharacterController.Move(movement * (moveSpeed * Time.deltaTime));
        }

        private void InitializeInput()
        {
            IPossessable possessable = gameObject.GetComponent<IPossessable>();
            possessable?.BindReceiver(this);
        }
    }
}