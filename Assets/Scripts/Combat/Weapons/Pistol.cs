﻿using Player;
using UnityEngine;

namespace Combat.Weapons
{
    /**
     * A weapon that can be fired each time the player fires the weapon (ignoring the fireRate
     */
    public class Pistol : Weapon
    {
        
        public override bool CanFire()
        {
            if (isReloading || ammoInClip <= 0)
            {
                return false;
            }

            return true;
        }

        protected override void Fire()
        {
            Transform ownerTransform = transform;
            FireRayBullet(ownerTransform.position, ownerTransform.forward);
            SetAmmoInClip(ammoInClip - 1);

            Debug.Log("Pistol fired!");
        }
    }
}