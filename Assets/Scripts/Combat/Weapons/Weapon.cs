using System;
using System.Collections;
using System.Collections.Generic;
using Player;
using UnityEngine;

namespace Combat.Weapons
{
    /**
     * Gives the gameObject the ability to fire bullets and deal damage to other gameObjects
     */
    public abstract class Weapon : MonoBehaviour, IInputReceiver
    {
        /** The minimum time the weapon must wait before being able to fire again after a successful fire */
        [SerializeField] protected float baseDamage = 1.0f;

        /** The minimum time the weapon must wait before being able to fire again after a successful fire */
        [SerializeField] protected float minSecondsBetweenShots = 1.0f;

        /** The total duration of this weapon's reload time */
        [SerializeField] protected float reloadDurationSeconds = 1.0f;

        /** The maximum ammo inside of a magazine */
        [SerializeField] protected int clipSize = 10;

        /** The current ammo loaded in the weapon (the bun can shoot this many times before needing to reload) */
        protected int ammoInClip = 10;
        
        /** If true, this weapon is currently being reloaded */
        protected bool isReloading = false;
        
        /** The world time at which this gun is allowed to fire again */
        protected float canFireAtTimestamp = 0.0f;

        /** Called each time the amount of bullets left in the clip changes */
        public event Action<int> OnAmmoCountChanged;
        
        /** Called when this gun started reloading */
        public event Action OnReloadStarted;
        
        /** Called when this gun has finished reloading */
        public event Action OnReloadFinished;

        //~ START Implementation IInputReceiver
        public void ReceiveButtonDown(in string inputName)
        {
            if (inputName == "Fire1")
            {
                TryFire();
            }
        }
        //~ END Implementation IPossessable
        
        //~ START Implementation MonoBehaviour
        protected void Start()
        {
            InitializeInput();
        }
        //~ END Implementation MonoBehaviour
        
        private void InitializeInput()
        {
            IPossessable possessable = gameObject.GetComponentInParent<IPossessable>();
            possessable?.BindReceiver(this);
        }

        /** Fires the gun if it is able to
         * @return - False if a bullet was not fired
         */
        public bool TryFire()
        {
            if ( ! CanFire())
            {
                return false;
            }

            canFireAtTimestamp = minSecondsBetweenShots + Time.time;
            Fire();
            OnAmmoCountChanged?.Invoke(ammoInClip);
            return true;
        }

        /** returns true if the weapon can fire */
        public virtual bool CanFire()
        {
            if (isReloading || Time.time < canFireAtTimestamp || ammoInClip < 0)
            {
                return false;
            }

            return true;
        }

        /** Starts reloading this weapon */
        public void Reload()
        {
            if (isReloading)
            {
                return;
            }

            Debug.Log("Start reload: '" + gameObject.name + "'");
            StartCoroutine(ReloadCoroutine());
        }

        /** Gets called when the weapon fires successfully. This should implement how bullets should be fired. */
        protected abstract void Fire();

        /** Fires a single line trace bullet
         * @note - Ammo is not reduced after calling this function!
         */
        protected List<GameObject> FireRayBullet(Vector3 startPosition, Vector3 fireDirection)
        {
            List<GameObject> hitGameObjects = new List<GameObject>();
            RaycastHit hit;

            bool hasHit = Physics.Raycast(startPosition, fireDirection, out hit);
            if (hasHit)
            {
                Debug.Log(gameObject.name + " has fired a ray and hit a thing: '" + hit.collider.gameObject.name);
                hitGameObjects.Add(hit.collider.gameObject);
            }

            return hitGameObjects;
        }

        /** Returns the ammo amount that the gun should be filled with after reloading has finished
         * @return - The ammo in the clip after a successful reload
         */
        protected virtual int GetReloadAmmoAmount()
        {
            return clipSize;
        }

        /** Sets the amount of ammo currently loaded in the clip
         * @param newAmount - The new amount loaded in the clip
         */
        protected void SetAmmoInClip(int newAmount)
        {
            if (ammoInClip == newAmount)
            {
                return;
            }

            ammoInClip = newAmount;
            OnAmmoCountChanged?.Invoke(ammoInClip);
        }

        /** Reloads this weapon */
        private IEnumerator ReloadCoroutine()
        {
            isReloading = true;
            OnReloadStarted?.Invoke();
            yield return new WaitForSeconds(reloadDurationSeconds);
            ammoInClip = GetReloadAmmoAmount(); // This assumes there's a method or constant for max ammo count
            isReloading = false;
            OnReloadFinished?.Invoke();
            OnAmmoCountChanged?.Invoke(ammoInClip);
        }
    }
}