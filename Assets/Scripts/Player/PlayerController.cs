﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Player
{
    
    /** Allows a gameObject to receive input from a player controller */
    public interface IPossessable
    {
        /** Called when a player controller starts possessing this object */
        virtual void OnPossessedBy(in PlayerController possessedBy) { }

        /** Called when a player controller stops possessing this object */
        virtual void OnUnPossessed() { }

        /** Returns a list of behaviours which are receiving input from this possessable */
        List<IInputReceiver> GetBoundReceivers();
        
        /** Adds a behaviour that should receive input from this possessable */
        void BindReceiver(in MonoBehaviour targetBehaviour);
        
        /** Removes a behaviour so it no longer receives input from this possessable */
        void UnbindBehaviour(in MonoBehaviour targetBehaviour);
        
        /** Called each update tick for each input currently active on the possessing player
         * @param inputName - The target input
         * @param isActive - True if the input button is currently pressed
         */
        void IsPlayerButtonDown(in string inputName, bool isActive);
        
        /** Called when the player pressed a button
         * @param inputName - The target input that just activated
         */
        void OnPlayerButtonDown(in string inputName);
        
        /** Called when the player releases a button
         * @param inputName - The target input that just de-activated
         */
        void OnPlayerButtonUp(in string inputName);
        
        /** Called each update tick for each input axis currently active on the possessing player
         * @param inputName - The target input
         * @param axisValue - The raw value of the axis
         */
        void ReceivePlayerAxis(in string inputName, float axisValue);
    }
    
    /** Catches player input, and directs the input to corresponding objects
     * @note - This behaviour is supposed to be the first entrypoint in the player's interaction with the game.
     *         For example, when the player presses the shoot button, it should only shoot the gun that the player
     *         character is holding. The player controller possesses a character so input is being sent to this
     *         character. The currently equipped gun listens to fire events from the character to which it was equipped.
     */
    public class PlayerController : MonoBehaviour
    {
        /** The player that will be spawned on Start */
        [SerializeField] GameObject defaultPlayerPrefab;
        
        //Ideally this would be retrieved from the input map defined in Unity's settings, but I couldn't find out how...
        /** The button inputs that this player controller will send to it's possessed object */
        [SerializeField] string[] boundInputButtonNames;
        /** The axis inputs that this player controller will send to it's possessed object */
        [SerializeField] string[] boundInputAxisNames;
        
        /** Called when this controller possesses a new object */
        public event Action<GameObject> OnPossessedObjectChanged;

        private GameObject _possessedObject;
        /** The object which input is currently being sent to */
        public GameObject PossessedObject
        {
            get => _possessedObject;
            
            protected set
            {
                if (_possessedObject == value)
                {
                    return;
                }
                _possessedObject = value;
                enabled = value != null;
                OnPossessedObjectChanged?.Invoke(_possessedObject);
            }
        }

        private void Start()
        {
            SpawnDefaultPlayer();
        }

        private void Update()
        {
            if (_possessedObject == null)
            {
                enabled = false;
            }
            SendInputTick(in _possessedObject);
        }

        private void SpawnDefaultPlayer()
        {
            if (defaultPlayerPrefab != null)
            {
                PossessedObject = Instantiate(defaultPlayerPrefab, transform.position, Quaternion.identity);
            }
        }

        private void Possess(ref GameObject possessTarget)
        {
            if (possessTarget.GetComponent<IPossessable>() == null)
            {
                Debug.LogError($"{gameObject.name} tried possessing {possessTarget.name}, but target does not implement IPossessable");
            }
            PossessedObject = possessTarget;
            Debug.Log($"{gameObject.name} has possessed: {PossessedObject.name}");
        }
        
        /** Sends all the current input states to an object
         * @note called on Update for the currently possessed object
         */
        private void SendInputTick(in GameObject targetGameObject)
        {
            IPossessable possessable = targetGameObject.GetComponent<IPossessable>();
            if (possessable == null)
            {
                Debug.LogError($"{gameObject.name}, tried to send input to {targetGameObject.name}, but target has no Possessable component!");
                enabled = false;
                return;
            }

            for (int iAxisName = 0; iAxisName < boundInputAxisNames.Length; iAxisName++)
            {
                SendAxisInputToPossessable(in possessable, in boundInputAxisNames[iAxisName]);
            }
            for (int iButtonName = 0; iButtonName < boundInputButtonNames.Length; iButtonName++)
            {
                SendButtonInputToPossessable(in possessable, in boundInputButtonNames[iButtonName]);
            }
        }

        /** Sends an axis input to a possessed object */
        private void SendAxisInputToPossessable(in IPossessable possessable, in string axisName)
        {
            possessable.ReceivePlayerAxis(in axisName, Input.GetAxisRaw(axisName));
        }

        /** Sends a button input to a possessed object */
        private void SendButtonInputToPossessable(in IPossessable possessable, in string buttonName)
        {
            possessable.IsPlayerButtonDown(buttonName, Input.GetButton(buttonName));
            if (Input.GetButtonDown(buttonName))
            {
                possessable.OnPlayerButtonDown(buttonName);
            }
            else if (Input.GetButtonUp(buttonName))
            {
                possessable.OnPlayerButtonUp(buttonName);
            }
        }
    }
}
