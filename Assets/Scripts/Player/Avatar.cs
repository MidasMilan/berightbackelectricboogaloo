﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Player
{
    
    /** Allows a behaviour to receive input from a player controller's possessed object */
    public interface IInputReceiver
    {
        virtual void ReceiveButton(in string inputName, bool isActive) { }
        virtual void ReceiveButtonDown(in string inputName) { }
        virtual void ReceiveButtonUp(in string inputName) { }
        virtual void ReceiveAxis(in string inputName, float axisValue) { }
    }
    
    /** A component that gives the gameObject the ability to be controlled by a controller
     * @note - This is supposed to be connected to anything that the player can control. It will send input received by
     *         the controller to other relevant behaviors so input is completely decoupled from actual behavior
     *         implementation 
     */
    public class Avatar : MonoBehaviour, IPossessable
    {
        //~ START Implementation IPossessable
        private List<IInputReceiver> _boundInputReceivers;

        public List<IInputReceiver> GetBoundReceivers()
        {
            return _boundInputReceivers;
        }

        public void BindReceiver(in MonoBehaviour targetBehaviour)
        {
            IInputReceiver receiver = (IInputReceiver)targetBehaviour;
            if (receiver == null)
            {
                Debug.LogError($"{gameObject.name} tried binding behaviour: {targetBehaviour.name}, but behaviour does not implement IInputReceiver");
                return;
            }
            if (_boundInputReceivers.Contains(receiver))
            {
                return;
            }
            _boundInputReceivers.Add(receiver);
        }

        public void UnbindBehaviour(in MonoBehaviour targetBehaviour)
        {
            IInputReceiver receiver = (IInputReceiver)targetBehaviour;
            if (receiver == null)
            {
                Debug.LogWarning($"{gameObject.name} tried unbinding behaviour: {targetBehaviour.name}, but behaviour does not implement IInputReceiver");
                return;
            }
            if ( ! _boundInputReceivers.Contains(receiver))
            {
                return;
            }
            _boundInputReceivers.Remove(receiver);
        }

        public void IsPlayerButtonDown(in string inputName, bool isActive)
        {
            foreach (IInputReceiver receiver in _boundInputReceivers)
            {
                receiver.ReceiveButton(inputName, isActive);
            }
        }

        public void OnPlayerButtonDown(in string inputName)
        {
            foreach (IInputReceiver receiver in _boundInputReceivers)
            {
                receiver.ReceiveButtonDown(inputName);
            }
        }

        public void OnPlayerButtonUp(in string inputName)
        {
            foreach (IInputReceiver receiver in _boundInputReceivers)
            {
                receiver.ReceiveButtonUp(in inputName);
            }
        }

        public void ReceivePlayerAxis(in string inputName, float axisValue)
        {
            foreach (IInputReceiver receiver in _boundInputReceivers)
            {
                receiver.ReceiveAxis(in inputName, axisValue);
            }
        }
        //~ END Implementation IPossessable

        //~ START Implementation MonoBehaviour
        private void Awake()
        {
            _boundInputReceivers = new List<IInputReceiver>();
        }
        //~ END Implementation MonoBehaviour
    }
}