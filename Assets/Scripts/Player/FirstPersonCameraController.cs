﻿using UnityEngine;
using UnityEngine.Serialization;

namespace Player
{

    /** Controls a camera and player rotation driven by the mouse input */
    public class FirstPersonCameraController : MonoBehaviour, IInputReceiver
    {
        /** The sensitivity of the aiming movement */
        [SerializeField] private float cameraAimSensitivity = 1000.0f;
        
        /** When true, the horizontal aiming is flipped so rotating right, will rotate left instead */
        [SerializeField] private bool isHorizontalAimingInverted = false;
        
        /** When true, the vertical aiming is flipped so rotating up, will rotate down instead */
        [SerializeField] private bool isVerticalAimingInverted = false;
        
        /** The transform of the character being controlled. Used to rotate the player object */
        [SerializeField] protected Transform characterTransform;
        
        /** The transform of the camera being controlled. Used to rotate the camera separately from the player object */
        [SerializeField] protected Transform cameraPivot;
        
        /** The current aim offset of the camera
         * @note - a separate value is used instead of directly using the camera transform, so it can still be clamped correctly
         */
        private float _verticalRotation = 0f;
        
        
        //~ START IInputReceiver implementation 
        public void ReceiveAxis(in string inputName, float axisValue)
        {
            if (inputName == "Mouse X")
            {
                RotatePlayerTick(axisValue);
                return;
            }
            if (inputName == "Mouse Y")
            {
                RotateCameraTick(axisValue);
                return;
            }
        }
        //~ END IInputReceiver implementation

        //~ START MonoBehaviour implementation
        void Start()
        {
            InitializeInput();
            InitializeCamera();
        }
        //~ END MonoBehaviour implementation

        private void InitializeInput()
        {
            IPossessable possessable = gameObject.GetComponent<IPossessable>();
            possessable?.BindReceiver(this);
        }

        private void InitializeCamera()
        {
            if (characterTransform == null)
            {
                Debug.LogError(
                    $"{gameObject.name} tried to initialize the FirstPersonCameraController, but characterTransform is null!");
                enabled = false;
                return;
            }
            if (cameraPivot == null)
            {
                Debug.LogError(
                    $"{gameObject.name} tried to initialize the FirstPersonCameraController, but cameraPivot is null!");
                enabled = false;
                return;
            }

            SetCursorLockState(CursorLockMode.Locked);
            _verticalRotation = cameraPivot.rotation.x;
        }

        public static void SetCursorLockState(CursorLockMode newLockMode)
        {
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = newLockMode != CursorLockMode.Locked;
        }

        private void RotatePlayerTick(float rotateInputDelta)
        {
            float rotateAmount = rotateInputDelta * cameraAimSensitivity * Time.deltaTime;
            if (isHorizontalAimingInverted)
            {
                rotateAmount *= -1;
            }

            characterTransform.Rotate(Vector3.up * rotateAmount);
        }

        private void RotateCameraTick(float rotateInputDelta)
        {
            float rotateAmount = rotateInputDelta * cameraAimSensitivity * Time.deltaTime;
            if (isVerticalAimingInverted)
            {
                rotateAmount *= -1;
            }

            _verticalRotation -= rotateAmount;
            _verticalRotation = Mathf.Clamp(_verticalRotation, -90f, 90f); // Clamp vertical rotation to avoid flipping

            cameraPivot.localRotation = Quaternion.Euler(_verticalRotation, 0f, 0f);
        }
    }
}